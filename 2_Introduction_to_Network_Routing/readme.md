# Introduction to Network Routing (CCNA2)
	
(0). Network Fundamentals
 
 1. Routing and Packet Forwarding
 1. Subnetting & Supernetting
 1. Static Routing
 1. Dynamic Routing
 1. Routing Information Protocol (RIP)
 1. Distance Vector Routing Protocols
 1. Enhanced Interior Gateway Routing Protocol (EIGRP)
 1. Link-State Routing Protocols
 1. Open Shortest Path First (OSPF)
 1. The Routing Table: A Closer Look
