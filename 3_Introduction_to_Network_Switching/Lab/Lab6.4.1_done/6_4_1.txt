enable se class
no ip domain-lo
ip de 172.17.99.1
line con 0
password cisco
login
line vty 0 15
password cisco
login
end
copy r s


int r fa0/11,fa0/18,fa0/6
sw mode access
no sh
ex


vtp mode server
vtp domain Lab6
vtp password cisco

vtp mode client
vtp domain Lab6
vtp password cisco

int r fa0/1-5
sw mode trunk
sw trunk native vlan 99
no sh

vlan 99
name management
vlan 10
name faculty-staff
vlan 20
name students
vlan 30
name guest


int vlan 99
ip address 172.17.99.11 255.255.255.0
no sh

int vlan 99
ip address 172.17.99.12 255.255.255.0
no sh

int vlan 99
ip address 172.17.99.13 255.255.255.0
no sh


int r fa0/6-10
sw access vlan 30
int r fa0/11-17
sw access vlan 10
int r fa0/18-24
sw access vlan 20
ex


h R1
no ip domain-lo
enable se cisco
line con 0
password cisco
login
line vty 0 15
password cisco
login

int fa0/0
no sh
int fa0/0.1
encap dot1q 1
ip addr 172.17.1.1 255.255.255.0
int fa0/0.10
encap dot1q 10
ip addr 172.17.10.1 255.255.255.0
int fa0/0.20
encap dot1q 20
ip addr 172.17.20.1 255.255.255.0
int fa0/0.30
encap dot1q 30
ip addr 172.17.30.1 255.255.255.0
int fa0/0.99
encap dot1q 99 native
ip addr 172.17.99.1 255.255.255.0

int fa0/1
ip addr 172.17.50.1 255.255.255.0
desc server interface
no sh
ex
