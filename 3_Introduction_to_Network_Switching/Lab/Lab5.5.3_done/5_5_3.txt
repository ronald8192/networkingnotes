S1:
spanning-tree vlan 10 priority 4096
spanning-tree vlan 20 priority 16384

S2:
spanning-tree vlan 20 priority 4096
spanning-tree vlan 30 priority 16384

S3:
spanning-tree vlan 30 priority 4096
spanning-tree vlan 10 priority 16384


============================================


int fa0/6
sw access vlan 30
sw mode access
no sh

int fa0/5
sw trunk native vlan 99
sw mode trunk
no sh

