conf t
enable se cimenable
line con 0
password cimconsole
login
line vty 0 15
password cimtelnet
login
exi



en
conf t
no ip domain-lo



int r fa0/1,fa0/19,fa0/21,fa0/23,gi1/1
sw mo trunk
sw trunk native vlan 50

int r fa0/20,fa0/22,fa0/24,gi1/1
sw mo trunk
sw trunk native vlan 50

int r fa0/23-24
int r fa0/21-22



sw mo trunk
sw trunk native vlan 50


vtp mode client
vtp domain cwcim
vtp password cisco



vlan 10
name account
vlan 20
name engineering
vlan 30
name marketing
vlan 40
name humanres
vlan 50
name native
vlan 99
name manage


int r fa0/2-6
sw mo access
sw access vlan 10
int r fa0/7-12
sw mo access
sw access vlan 20
int r fa0/13-18
sw mo access
sw access vlan 30
int r fa0/20,fa0/22,fa0/24
sw mo access
sw access vlan 40
ex


int r fa0/1-6
sw mo access
sw access vlan 10
int r fa0/7-12
sw mo access
sw access vlan 20
int r fa0/13-18
sw mo access
sw access vlan 30
int r fa0/19,fa0/21,fa0/23
sw mo access
sw access vlan 40
ex


int r fa0/1-8
sw mo access
sw access vlan 10
int r fa0/9-16
sw mo access
sw access vlan 20
int r fa0/17-22
sw mo access
sw access vlan 30
ex

int r fa0/1-8
sw mo access
sw access vlan 10
int r fa0/9-16
sw mo access
sw access vlan 20
int r fa0/17,fa0/20,fa0/23-24
sw mo access
sw access vlan 30
ex


int r fa0/1-8
sw mo access
sw access vlan 10
int r fa0/9-16
sw mo access
sw access vlan 20
int r fa0/17-22
sw mo access
sw access vlan 30
ex

===================

int vlan 50
no ip addr
ex


=====================

int vlan 99
ip addr 192.168.99.1 255.255.255.0
ex
ip default-gate 192.168.99.254

int vlan 99
ip addr 192.168.99.2 255.255.255.0
ex
ip default-gate 192.168.99.254

int vlan 99
ip addr 192.168.99.3 255.255.255.0
ex
ip default-gate 192.168.99.254

int vlan 99
ip addr 192.168.99.4 255.255.255.0
ex
ip default-gate 192.168.99.254

int vlan 99
ip addr 192.168.99.5 255.255.255.0
ex
ip default-gate 192.168.99.254

=====================



as1,3:
int r fa0/1-22
sw po
sw po max 1
sw po mac- stic
sw po violation sh
ex

as2:
int r fa0/1-17,fa0/20,fa0/23-24
sw po
sw po max 1
sw po mac- stic
sw po violation sh
ex


int fa0/0
no sh
ex

int fa0/0.10
encap dot1q 10
ip address 192.168.10.254 255.255.255.0

int fa0/0.20
encap dot1q 20
ip address 192.168.20.254 255.255.255.0

int fa0/0.30
encap dot1q 30
ip address 192.168.30.254 255.255.255.0

int fa0/0.40
encap dot1q 40
ip address 192.168.40.254 255.255.255.0

int fa0/0.50
encap dot1q 50 native
ip address 192.168.50.254 255.255.255.0

int fa0/0.99
encap dot1q 99
ip address 192.168.99.254 255.255.255.0
ex

int vlan 99
ip address 192.168.99.1 255.255.255.0
no sh
ex












