# Accessing WAN (CCNA4)

 1. Introduction to WANs
    1. Providing Integrated Services to the Enterprise
    1. WAN technology overview
    1. WAN Connection Options
 1. Point-to-Point Protocol (PPP)
 1. Frame Relay
    1. Basic Frame Relay Concepts
    1. Configuring a Frame Relay
    1. Advanced FR Concepts
    1. Configuring Advanced FR
 1. Network Security
    1. Enterprise Networks Security Threats
    1. Basic Router Security
 1. Access Control List
 1. Providing Teleworker Services
 1. Implementing IP Addressing Services
 1. Network Troubleshooting
