R1(config)#access-list 10 deny 192.168.10.0 0.0.0.255
R1(config)#access-list 10 permit any
R1(config)#interface fa0/1
R1(config-if)#ip access-group 10 out

R2(config)#access-list 11 deny 192.168.11.0 0.0.0.255
R2(config)#access-list 11 permit any
R2(config)#interface s0/1/0
R2(config-if)#ip access-group 11 out


R3(config)#ip access-list standard NO_ACCESS
R3(config-std-nacl)#deny host 192.168.30.128
R3(config-std-nacl)#permit any
R3(config)#interface fa0/0
R3(config-if)#ip access-group NO_ACCESS in