no ip domain-lo
enable sec cisco
banner motd asdfasdf
line con 0
password cisco
login

!R1
int fa0/0
ip addr 192.168.10.1 255.255.255.0
no sh
int s0/0/1
ip addr 10.1.1.1 255.255.255.252
no sh

!R2
int s0/0/1
ip addr 10.1.1.2 255.255.255.252
no sh
int lo0
ip addr 209.165.200.225 255.255.255.224


!S1
ho S1
int vlan 1
ip addr 192.168.10.2 255.255.255.0
ip default-gateway 192.168.10.1

!R1
router eigrp 1
network 192.168.10.0 0.0.0.255
network 10.1.1.0 0.0.0.3

!R2
router eigrp 1
network 10.1.1.0 0.0.0.3
network 209.165.200.224 0.0.0.31


!R1
int s0/0/1
encap frame-relay
frame-relay map ip 10.1.1.2 102 broadcast
frame-relay map ip 10.1.1.1 102
no sh

!R2
int s0/0/1
encap frame-relay
frame-relay map ip 10.1.1.1 201 broadcast
frame-relay map ip 10.1.1.2 201
no sh


!R1
int s0/0/1.112 point-to-point
ip addr 10.1.1.5 255.255.255.252
frame-relay interface-dlci 112

!R2
int s0/0/1.212 point-to-point
ip addr 10.1.1.6 255.255.255.252
frame-relay interface-dlci 212


