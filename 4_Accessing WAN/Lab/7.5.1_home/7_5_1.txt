line con 0
password cisco
login
line vty 0 4
password cisco
login
enable sec cisco

!R1
hostname R1
banner motd "AUTHORIZED ACCESS ONLY!"
int Fa0/0
ip addr 192.168.10.1 255.255.255.0
no sh
int Fa0/1
ip addr 192.168.11.1 255.255.255.0
no sh
int S0/0/0
ip addr 10.1.1.1 255.255.255.252
clock rate 64000
no sh

!R2
hostname R2
banner motd "AUTHORIZED ACCESS ONLY!"
int Fa0/0
ip addr 192.168.20.1 255.255.255.0
no sh
int S0/0/0
ip addr 10.1.1.2 255.255.255.252
no sh
int S0/0/1
ip addr 10.2.2.1 255.255.255.252
clock rate 64000
no sh
int S0/1/0
ip addr 209.165.200.225 255.255.255.224
no sh

!R3
hostname R3
banner motd "AUTHORIZED ACCESS ONLY!"
int Fa0/0
ip addr 192.168.30.1 255.255.255.0
no sh
int S0/0/1
ip addr 10.2.2.2 255.255.255.252
no sh


!R1
username R2 password 0 cisco123
int s0/0/0
encap ppp
ppp auth chap

!R2
username R1 password 0 cisco123
int s0/0/0
encap ppp
ppp auth chap

username R3 password 0 cisco123
int s0/0/1
encap ppp
ppp auth chap

!R3
username R2 password 0 cisco123
int s0/0/1
encap ppp
ppp auth chap


!R1
router ospf 1
network 192.168.10.0 0.0.0.255 area 0
network 192.168.11.0 0.0.0.255 area 0
network 10.1.1.0 0.0.0.3 area 0
passive-interface fa0/0
passive-interface fa0/1
exit

!R2
ip route 0.0.0.0 0.0.0.0 209.165.200.226
router ospf 1
net 192.168.20.1 0.0.0.255 area 0
net 10.1.1.0 0.0.0.3 area 0
net 10.2.2.0 0.0.0.3 area 0
passive-interface fa0/0
passive-interface s0/1/0
default-information originate
exit

!R3
router ospf 1
net 192.168.30.1 0.0.0.255 area 0
net 10.2.2.0 0.0.0.3 area 0
passive-interface fa0/1
exit

!ISP
ip route 0.0.0.0 0.0.0.0 s0/0/0


!R1
ip dhcp ex 192.168.10.1 192.168.10.9
ip dhcp ex 192.168.11.1 192.168.11.9
ip dhcp pool R1LAN1
network 192.168.10.0 255.255.255.0
default-router 192.168.10.1
dns-server 192.168.20.254
exit
ip dhcp pool R1LAN2
network 192.168.11.0 255.255.255.0
default-router 192.168.11.1
dns-server 192.168.20.254

!R3
ip dhcp ex 192.168.30.1 192.168.30.9
ip dhcp pool R3LAN
network 192.168.30.0 255.255.255.0
default-router 192.168.30.1
dns-server 192.168.20.254
exit

!DNS
www.cisco.com	A record	209.165.201.30

!R2
ip access-list standard R2NAT
permit 192.168.10.0 0.0.0.255
permit 192.168.20.0 0.0.0.255
permit 192.168.30.0 0.0.0.255
permit 192.168.11.0 0.0.0.255
exit

ip nat inside source static 192.168.20.254 209.168.202.131
int s0/1/0
ip nat outside
int fa0/0
ip nat inside
int s0/0/0
ip nat inside
int s0/0/1
ip nat inside 

ip nat pool R2POOL 209.165.202.129 209.165.202.130 netmask 255.255.255.252
ip nat inside source list R2NAT pool R2POOL overload

ip route 209.168.202.128 255.255.255.224 s0/0/0