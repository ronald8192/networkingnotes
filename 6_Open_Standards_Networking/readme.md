# Open Standards Networking

 1. Introduction of Open Standards
    1. Common open standards for networking devices
    1. Current market share of enterprise networking devices
    1. Benefits and challenges of multi vendor networking environment
    1. Using networking devices from different vendors
 1. Virtual VLANs
    1. IEEE802.1q conforming VLAN tagging and configuration
    1. VLAN management with GVRP
    1. Inter-VLAN routing with layer 3 switches
 1. Network Redundancy
    1. Review of hierarchical design
    1. Usage and configuration of link aggregation with LACP
    1. Implementation of RSTP and MSTP
    1. Virtual Router Redundancy Protocol (VRRP)
    1. Benefits of using stackable switches
 1. IPv4 Routing
    1. Basic Routing
    1. Single area OSPF interworking in a multivendor environment
    1. Basic BGP concepts and configuration
    1. Routing policy fundamentals
    1. IP Addressing
    1. WAN Connections
 1. Network Security and Network Monitoring
    1. Packet Filtering
    1. Network Address Translation (NAT)
    1. Authentication, Authorization and Accounting with IEEE802.1x
    1. Basic network monitoring with SNMP
 1. Virtualization Technologies in Networking
    1. What is Network Virtualization?
    1. Virtualization of Network Devices
    1. Virtualization of Network Transports
    1. Service Edge Virtualization
    1. Network Services in a NVE
    1. Software Defined Networking (SDN)
