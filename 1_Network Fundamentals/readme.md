# Network Fundamentals (CCNA1)

 1. Exploring the Network
 1. Configuring a Network Operating System (Cisco IOS)
 1. Network Protocols and Communications
 1. Ch4 - Layer 1 + 2
    1. (4a) Physical Layer
    1. (4b) Data Link Layer
 1. Ethernet
 1. Network Layer
 1. Transport Layer
 1. Ch8 - IPv4, IPv6
    1. IPv4
    1. IPv6
 1. Subnetting IP Networks
 1. Application Layer
 1. Managing A Small Network
 1. Wireless
