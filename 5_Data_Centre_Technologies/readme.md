# Data Centre Technologies

## Chapter related to networking:

 1. --
 1. --
 1. Network Technologies
   * Hierarchical Network Design - 3 Tier Model
   * Redundancy with STP
   * Link Aggregation
     * Etherchannel
   * First Hop Redundancy Protocols (FHRP)
   * Hot Standby Router Protocol (HSRP)
   * Interface Tracking
   * Load Sharing with HSRP - MHSRP
   * Virtual Router Redundancy Protocol (VRRP)
   * Gateway Load Balancing Protocol (GLBP)
 
 1. Contemporary Network Technologies for Data Centres
   * Edge Virtual Bridging
   * Virtual Ethernet Bridging (VEB)
   * Distributed Virtual Switches
   * Virtual Ethernet Port Aggregator (VEPA)
     * Standard Mode VEPA
     * Multi-Channel VEPA
   * Virtual Bridge Port Extension
   * Single root I/O virtualization (SR-IOV) interface
   * Multipath Ethernet
   * Stacking / Multi-Chassis Link Aggregation (MLAG)
     * HP (H3C) Stacking Technologies – Intelligent Resilient Framework (IRF)
     * Juniper Virtual Chassis Technology
     * Cisco (Stacking) Technologies
   * Layer 2 Multipath (L2MP)
   * Transparent Interconnection of Lots of Links (TRILL)
   * 802.1aq Shortest Path Bridging (SPB)
   * Virtual eXtensible LAN (VXLAN)
   * Network Virtualization using Generic Routing Encapsulation (NVGRE)
   * Stateless Transport Tunneling (STT)
   * Layer 2 Extension between Data Centres
   * Dense Wavelength-division Multiplexing (DWDM)
   * Pseudowire
   * Virtual Private LAN Service (VPLS)
   * Software Defined Networking (SDN)
   * OpenFlow
